# -*- coding: utf-8 -*-
# This file is part of Aspiturf.
# Aspiturf is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
# Aspiturf is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
# You should have received a copy of the GNU General Public License along with Aspiturf.  If not, see <https://www.gnu.org/licenses/>.

import scrapy


class MyscraperItem(scrapy.Item):
    # define the fields for your item here like:
    url = scrapy.Field()
    html = scrapy.Field()
    arriv = scrapy.Field()
    prixnom = scrapy.Field()
    hippo = scrapy.Field()
    reun = scrapy.Field()
    prix = scrapy.Field()
    sex = scrapy.Field()
    handi = scrapy.Field()
    meteo = scrapy.Field()
    tempscourse = scrapy.Field()
    corde = scrapy.Field()
    dist = scrapy.Field()
    lice = scrapy.Field()
    typec = scrapy.Field()
    reclam = scrapy.Field()
    courseabc = scrapy.Field()
    # tempstot = scrapy.Field()
    autos = scrapy.Field()
    cheque = scrapy.Field()
    natpis = scrapy.Field()
    condi = scrapy.Field()
    pistegp = scrapy.Field()
    groupe = scrapy.Field()
    id = scrapy.Field()
    comp = scrapy.Field()
    jour = scrapy.Field()
    partant = scrapy.Field()
    age = scrapy.Field()
    europ = scrapy.Field()
    amat = scrapy.Field()
    th = scrapy.Field()
    alll = scrapy.Field()
    heure = scrapy.Field()
    numero = scrapy.Field()
    musiquept = scrapy.Field()
    quinte = scrapy.Field()
    temperature = scrapy.Field()
    forceVent = scrapy.Field()
    directionVent = scrapy.Field()
    nebulositeLibelleCourt = scrapy.Field()

    caractrap_id = scrapy.Field()
    rapport_type = scrapy.Field()
    pari_type = scrapy.Field()
    horses = scrapy.Field()
    rapport = scrapy.Field()
    # info = scrapy.Field()
    # pass
